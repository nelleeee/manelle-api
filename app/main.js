const express = require("express");
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const port = 3030;

// BodyParser dépendance anciennement native à express, qui me converti les entrées (posts) à un format choisi, ici : json
const jsonParser = bodyParser.json();

// Même utilité que l'access control origin à l'exception que l'on doit l'écrire qu'une seule fois pour tous les onPoints
app.use(cors())



const messages = [
   {
      title: 'titre du message 1', 
      content: 'contenu du msg 1',
      isRead: false,
      id: 1
   },
   {
      title: 'titre du message 2',
      content: 'contenu du msg 2',
      isRead: false,
      id: 2
   },
 ];


app.get("/", (req, res) => res.send("Hello World!"));


// 
app.get("/messages", (req,res) => {

   // .json traite la réponse reçue au format d'objets json
   res.json(messages);
   res.status(200).send('Successfully Printed')
})




app.get("/messages/:id", (req, res) => {

   // req.params permet d'acceder au paramètres de la requête, ici :id est le paramètre.
   console.log(req.params);

   let message = {};

   // sur la liste de messages si le paramètre defini dans l'url est égal à un des id présents sur la liste, je renvoie le message
   for (let index = 0; index < messages.length; index++) {
      if (req.params.id == messages[index].id) {
         message = messages[index];
         break;
      } else {
         res.status(404).send('No message at', req.params.id)
      }
   }
   res.json(message);
   res.status(200).send('Successfully Printed')
})

// Don d'un id pas déjà présent dans la liste de messages pour plus tard pouvoir effectuer le delete sur un id précis
app.post("/messages", jsonParser, (req, res) => {
   console.log(req.body);
  let message = req.body;

   let newId = 1;

   for (let index = 0; index < messages.length; index++) {
      if (messages[index].id > newId) {
         newId = messages[index].id
      }
   }

   newId = newId + 1;
   message.id = newId;

   messages.push(message);
   console.log(messages);
   res.json(message);
})


app.delete("/messages/:id", (req, res) => {

   // mis à -1 pour que l'indexTodelete ne commence pas sur un id existant 
   let indexToDelete = -1;

   // boucle sur chaque index du tableau afin de comparer chaque id avec celui de la requête (celui du param :id)
   for (let index = 0; index < messages.length; index++) {
      if (messages[index].id == req.params.id) {
         indexToDelete = index;
         break;
      }
   }

   // verif + retire le msg à l'indexToDelete
   if (indexToDelete == -1) {
      res.status(404).send('Not found');
   } else {
      messages.splice(indexToDelete, 1);
   }

   res.status(200).send('Message Successfully Deleted!')
   
})
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
